---
title: "Plenbot"
date: 2022-09-17T00:29:21+02:00
draft: false
---

## Qu'est-ce que le Plenbot ?

Plenbot est un logiciel développé par [Plenyx (Hardstuck)](https://www.twitch.tv/plenyx) qui permet aux joueurs de GW2 d’installer ARCDps, d’envoyer automatiquement des logs en ligne et de notifier discord/twitch ou autre..

## Téléchargement et installation du Plenbot

Déjà, créer un dossier **BOT** dans le répertoire d’installation du jeux (__par défaut, C:/programmes/Guild Wars 2/__)  puis placer dedans le logiciel téléchargeable en haut à droite à l’adresse https://plenbot.net/uploader/ puis en cherchant le fichier  PlenBotLogUploader.exe.

{{< highlight html >}}
Il est possible qu’une alerte de sécurité arrive, puisque Windows n’est pas fan de télécharger des .exe sur internet :) 
{{< /highlight >}}

![Alerte Windows](/1.png)
![Alerte Windows](/2.png)

## Configurer Plenbot

Au premier lancement, il va demander plein de questions, indiquez lui que vous n’utilisez pas Plenbot pour twitch dans un premier temps

![Config 1](/3.png)

![Config 2](/4.png)

![Config 3](/5.png)