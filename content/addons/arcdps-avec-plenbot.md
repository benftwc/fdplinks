---
title: "Arcdps Avec Plenbot"
date: 2022-09-17T00:29:37+02:00
draft: false
---


## C'est quoi ARCDPS ?
Arcdps est l'addon de suivi de stats principal sur GW2 (aka Kikimeter)

![ARCDps](/9.png)

## Avertissement

{{< highlight html >}}
AVERTISSEMENT NCSOFT : 

LA MODIFICATION DE GUILD WARS 2 VIA DES OUTILS TIERS N'EST PAS PRIS EN CHARGE PAR ARENANET OU NCSOFT.
NE PAS CONTACTER L'ASSISTANCE AU SUJET DES PROBLÈMES CLIENTS DU JEU LORS DE L'UTILISATION DE CET OUTIL. 
IL N'Y A AUCUNE AIDE OU GARANTIE.
CECI EST ENTIÈREMENT À VOS PROPRES RISQUES ET VOUS ASSUMEZ TOUTE RESPONSABILITÉ.

{{< /highlight >}}

[> Use of Third Party Programs for Technical Support](https://support.ncsoft.com/hc/en-us/articles/115005949466-Use-of-Third-Party-Programs-for-Technical-Support)
## Installation de ARCDPS
Clique sur **ARCDPS PLPUGIN MANAGER**, indique ensuite le dossier où est installé **gw2-64.exe** (par défaut, dans __C:/programmes/Guild Wars 2/__)

![GW2.exe](/6.png)

Coche **ENABLE MODULE**, selectionne tout les addons sauf **SCROLLING TEXT** puis clique sur **CHECK NOW**

![Configurer la liste des addons](/7.png)

Vérifie que tout est installé, dans le dossier où est placé ton GW2-64.exe, tu as du voir apparaitre plein de fichiers d3d11.dll .. s’ils sont là, tout est bien installé !

![Vérifie que tout va bien](/8.png)


## Configuration en jeux
Attention, **IL NE FAUT PAS** activer le rendu DX9, sinon arcdps **NE FONCTIONNERA PAS**

Site de arcdps : [https://www.deltaconnected.com/arcdps/](https://www.deltaconnected.com/arcdps/)

## Un petit guide vidéo si jamais

{{< youtube 0QoOL8x-0MM >}}