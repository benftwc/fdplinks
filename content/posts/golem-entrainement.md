---
title: "Golem Entrainement"
date: 2022-09-17T00:30:04+02:00
draft: false
---

## Introduction

Il te faudra être ou avoir créer une escouade pour réussir à rentrer dans l’instance “Zone d’entraînement des forces spéciales” Une fois l’intérieur il te faudra régler ton golem ainsi que de régler ton personnage aussi ! Tu pourra trouver ces consoles au fond de la salle  : 

Avec ce guide, on t'apprends à gérer tes <acronym title="Buff / améliorations personnelles">boons</acronym>


#### Les consoles au fond de la salle
![Les consoles au fond de la salle](/golem_1.png "Les consoles au fond de la salle")

#### La console de l'arène
![Console de l’arène](/golem_2.png "Console de l’arène")

#### Générateur de golems
![Générateur de golem](/golem_3.png "Générateur de golem")

## Boons Perso 
### Offensif

* Pouvoir (25 stacks)
* Fureur
### Défensif

* Protection 
* Résolution 
* Egide

### Utilitaire

* Alacrité
* Célérité
* Regeneration
* Rapidité 
* vigueur


## Réglages golem

### Taille

Moyen de façon à être au plus proche de la plupart des boss de raid

### Vie

4.000.000 de point de vie

### Altérations

* Saignement 
* Brûlure 
* Confusion 
* Poison 
* Tourment 
* Givré 
* Infirmité 
* Lent 
* Vulnérabilité (25 stacks)
* Faiblesse

En ce qui concerne un test de DPS Condition (Brûlure par exemple) il suffit de l’enlever et d’en mettre une autre a la place, le mieu etant de rester sur 10 altérations de façon à être grandeur nature les boss de raid auront la plupart du temps 10 altérations dans les groupe normaux

Tu n’as plus qu’à utiliser tes consommables et tout envoyer sur le golem, bien sûr après avoir pris connaissance de la rotation de sort à effectuer pour ta classe que tu pourras aisément retrouver sur le site [SnowCrows](https://snowcrows.com/builds?profession=any&category=meta)

## TADAM
#### Le golem avec ses altérations

![Altérations sur le golem](/golem_4.png "Altérations sur le golem") 

#### Et ton perso dopé aux bon boons !

![Boons sur le perso](/golem_5.png "Boons sur le perso")

## Note

Attention si tu démarre le combat de façon non intentionnel et que tu n’étais pas prêt ton dps risque d’être vraiment négativement influencer, soit sur d’être vraiment prêt, dans le cas contraire il te suffira de retourner sur la console du golem, demander à ce qu’il soit supprimé et dans la foulé de demander de recréer le golem d’avant ! C’est parti !