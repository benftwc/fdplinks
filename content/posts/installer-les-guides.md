---
title: "Comment bien installer les différents addons / mods ?"
date: 2022-09-17T00:29:37+02:00
draft: false
---

## Première étape

[Installer Plenbot](/addons/plenbot/)

## Seconde étape

[Installer ARCDPS avec Plenbot](/addons/arcdps-avec-plenbot/)

## Dernière étape

[Valider l'installation de ARCDPS sur le golem](/posts/golem-entrainement/)